// Array1. n natural soni berilgan. 2 sonining dastlabki n ta darajasidan tashkil topgan massivni qaytaruvchi getLevel2(n) nomli funksiya tuzing.

// function darajasi(n) {
//     let natija = [];
//     let d = 1;
//     for (let i = 1; i <= n; i++) {
//       d *= 2;
//       natija.push(d);
//     }
    
//     return natija;
// }

// console.log(darajasi(5)); 

// Array2. n natural soni va A, B butun sonlari berilgan (n > 2). a[0] = A; a[1] = B; boshqa elementlari o'zidan oldingi barcha elementlari yig'indisiga teng bo'lgan massivni hosil qiling va elementlarini chiqaring.

// function Massiv(n, a, b) {
//     const br_arr = [a, b];
//     let sum = a + b;
//     for (let i = 2; i < n; i++) {
//     br_arr.push(sum);
//     sum += br_arr[i - 1];
//     }
//     return br_arr;
// }
// console.log(Massiv(5, 2, 3));

  
// Array3. n ta elementdan tashkil topgan massiv berilgan. Uning elementlarini teskari tartibda chiqaruvchi programma tuzilsin.

// function reverse(arr) {
//     let result = [];
//     for (let i = arr.length - 1; i >= 0; i--) {
//       result.push(arr[i]);
//     }
//     return result;
// }
// var arr = [1,2,3,4,5];
// var result = reverse(arr);
// console.log(result); 


// Array4. n ta elementdan tashkil topgan massiv berilgan. Massiv elementlari orasidan toqlarini indekslari o'sish tartibida chiqaruvchi va ularning sonini chiqaruvchi programma tuzilsin.

// function NumberArr(arr) {
//     let oddArr = [];
//     let count = 0;
//     for (let i = 0; i < arr.length; i++) {
//       if (arr[i] % 2 !== 0) {
//         oddArr.push(arr[i]);
//         count++;
//       }
//     }
//     console.log(oddArr);
//     console.log("Toqlar sonlar =", count, "Ta");
// }
// NumberArr([4, 5, 7, 8, 6, 9]);


// Array5. n ta elementdan tashkil topgan massiv berilgan. Dastlab massiv elementlari orasidan juftlarini indekslari o'sish tartibida chiqaruvchi, keyin massiv elementlari orasidan toqlarini indekslari kamayish tartibida chiqaruvchi programma tuzilsin.

// function saral_array(arr) {
//     let evenArray = [];
//     let oddArray = [];
//     let result = [];
//     // juft va toq sonlarni ayirib oddArray va evenArray massivlarga joylashtiriladi
//     for (let i = 0; i < arr.length; i++) {
//       if (arr[i] % 2 === 0) {
//         evenArray.push(arr[i]);
//       } else {
//         oddArray.push(arr[i]);
//       }
//     }
//     // juftlarini indekslari o'sish tartibida qilish
//     for (let i = 0; i < evenArray.length; i++) {
//       result.push(evenArray[i]);
//     }
//     // toqlar indekslari kamayish tartibida qilish
//     for (let i = oddArray.length - 1; i >= 0; i--) {
//       result.push(oddArray[i]);
//     }
//     console.log(result);
// }
// saral_array([4, 5, 7, 8, 6, 9]);   


// Array6. n ta elementdan tashkil topgan massiv berilgan (n juft son). Massiv elementlari orasidan quyidagilarini chiqaruvchi programma tuzilsin. A[0], A[2], A[4], ... Shart operatori ishlatilmasin.

// function IndxElemn(arr) {
//     for (let i = 0; i < arr.length; i += 2) {
//       console.log(arr[i]);
//     }
// }
// const myArr = [3, 6, 2, 8, 9, 1];
// IndxElemn(myArr);
  

// Array7 n ta elementdan tashkil topgan massiv berilgan (n toq son). Massiv elementlari orasidan quyidagilarini chiqaruvchi programma tuzilsin. A[n-1], A[n-3], ... A[1]. Shart operatori ishlatilmasin.

// function IndexElemn(arr) {
//     for (let i = arr.length - 1; i >= 0; i -= 2) {
//       console.log(arr[i]);
//     }
// }
// const myArr = [3, 6, 2, 8, 9, 1];
// IndexElemn(myArr); 
  
// Array8. n ta elementdan tashkil topgan massiv berilgan. Dastlab massiv elementlari orasidan juft indekslilarini keyin toq indekslilarini chiqaruvchi programma tuzilsin. A[0], A[2], A[4], ... A[1], A[3], A[5],.... Shart operatori ishlatilmasin.

// function IndexElemn(arr) {
//     const evenIn = [];
//     const oddIn = [];
//     for (let i = 0; i < arr.length; i++) {
//       if (i % 2 === 0) {
//         evenIn.push(arr[i]);
//       } else {
//         oddIn.push(arr[i]);
//       }
//     }
//     console.log(...evenIn, ...oddIn);
// }
// const myArr = [3, 6, 2, 8, 9, 1];
// IndexElemn(myArr); 


// Array9. n ta elementdan tashkil topgan massiv berilgan (n juft son). Dastlab massiv elementlari orasidan toq indekslilarini o'shish tartibida keyin juft indekslilarini kamayish tartibida chiqaruvchi programma tuzilsin. A[1], A[3], A[5],.. A[6], A[4], A[2], A[0]. Shart operatori ishlatilmasin.

// function Arra(arr) {
//     let oddIn = [];
//     let evenIn = [];
//     for (let i = 0; i < arr.length; i++) {
//       if (i % 2 === 0) {
//         evenIn.push(i);
//       } else {
//         oddIn.push(i);
//       }
//     }
//     oddIn.sort((a, b) => arr[b] - arr[a]);
//     evenIn.sort((a, b) => arr[a] - arr[b]);
//     let sortedArray = [];
//     for (let i = 0; i < oddIn.length; i++) {
//       sortedArray.push(arr[oddIn[i]]);
//     }
//     for (let i = 0; i < evenIn.length; i++) {
//       sortedArray.push(arr[evenIn[i]]);
//     }
//     return sortedArray;
// }  
// let arr = [1, 2, 3, 4, 5, 6];
// console.log(Arra(arr)); 

// Array10. n ta elementdan tashkil topgan massiv berilgan. Massiv elementlarini quyidagicha chiqaruvchi programma tuzilsin. A[0], A[1], A[n-1], A[n-2], A[3], A[4], A[n-3], A[n-4], ...

// function cOrder(arr) {
//     let n = arr.length;
//     let output = [];
//     for (let i = 0; i < n; i += 2) {
//       output.push(arr[i]);
//     }
//     let j = n % 2 === 0 ? n - 1 : n - 2;
//     while (j >= 1) {
//       output.push(arr[j]);
//       j -= 2;
//     }
//     return output;
// }
// let arr = [1, 2, 3, 4, 5, 6];
// console.log(cOrder(arr)); 
  

// Array11. N ta elementdan tashkil topgan arr nomli massiv va K, L butun sonlari berilgan. (0 <= K <= L < N). Massivning K va L indekslari orasidagi elementlaridan tashqari elementlari yig'indisini qaytaruvchi rangeOutSum(arr) nomli funksiya tuzilsin.

// function rangeOutSum(arr, K, L) {
//     let sum = 0;
//     for (let i = 0; i < arr.length; i++) {
//       if (i < K || i > L) {
//         sum += arr[i];
//       }
//     }
//     return sum;
// }
// function deleteElementsBetween(arr, K, L) {
//     arr.splice(K + 1, L - K - 1);
//     return arr;
// }
// function rangeOutSum(arr, K, L) {
//     let sum = 0;
//     for (let i = 0; i < arr.length; i++) {
//       if (i < K || i > L) {
//         sum += arr[i];
//       }
//     }
//     arr.splice(K + 1, L - K - 1);
//     return [sum, arr];
// }
// let arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];
// let K = 2, L = 6;
// let [sum, newArr] = rangeOutSum(arr, K, L);
// console.log(`Range out sum: ${sum}`);
// console.log(`New array: ${newArr}`);
        

// Array12. Massivga kiritilgan qiymatlardan truthy va falsy elementlaridan iborat alohida 2 ta massiv hosil qiling.

// function Truthy_Falsy(arr) {
//     var truthy = [];
//     var falsy = [];
//     for (var i = 0; i < arr.length; i++) {
//       if (arr[i]) {
//         truthy.push(arr[i]);
//       } else {
//         falsy.push(arr[i]);
//       }
//     }
//     return {truthy: truthy, falsy: falsy};
// }
// var arr = [10, false, " ", "Samariddin", null];
// var result = Truthy_Falsy(arr);
// console.log("Truthy:", result.truthy);
// console.log("Falsy:", result.falsy);


// Array13. n ta elementdan tashkil topgan arr nomli massiv berilgan. Massiv juft indeksli elementlari orasidan kichigini aniqlovchi getOddMin(arr) nomli funksiya tuzilsin.

// function Min(arr) {
//     let min = Infinity;
//     for (let i = 0; i < arr.length; i += 2) {
//       if (arr[i] < min && arr[i] % 2 !== 0) {
//         min = arr[i];
//       }
//     }
//     return min === Infinity ? null : min;
// }
// const arr = [5, 2, 7, 4, 9, 6, 3, 8];
// console.log(Min(arr));


// Array14. n ta elementdan tashkil topgan arr nomli massiv berilgan. Massiv toq indeksli elementlari orasidan kattasini aniqlovchi getEvenMax(arr) tuzilsin.

// function Max(arr) {
//     let max = -Infinity;
//     for (let i = 0; i < arr.length; i += 2) {
//       if (arr[i] > max) {
//         max = arr[i];
//       }
//     }
//     return max;
// }
// let arr = [1, 7, 2, 9, 4, 6, 8, 3];
// console.log(Max(arr)); 

// Array15. n ta elementdan tashkil topgan massiv berilgan. Massiv elementlari orasidan oxirgi lokal maksimum elementi indeksini chiqaruvchi programma tuzilsin. Lokal maksimum - o'ng va chap qo'shinisidan katta bo'lgan element.

// function MaxIndex(arr) {
//     if (arr.length < 3) {
//       return -1;
//     }
//     for (let i = arr.length - 1; i > 0; i--) {
//       if (arr[i] > arr[i - 1]) {
//         return i;
//       }
//     }
//     return -1;
// }
// let arr = [1, 2, 5, 3, 1];
// console.log(MaxIndex(arr));

// Array16. n ta elementdan tashkil topgan massiv va R butun soni berilgan. Massiv elementlari orasidan R soniga eng yaqin sonni topuvchi programma tuzilsin.

// function Mas(arr, R) {
//     let clos = Infinity;
//     let index = -1;
  
//     for (let i = 0; i < arr.length; i++) {
//       const diff = Math.abs(arr[i] - R);
  
//       if (diff < clos) {
//         clos = diff;
//         index = i;
//       }
//     }  
//     return index;
// }
// const arr = [1, 3, 7, 10, 15, 20];
// const R = 12;

// console.log(Mas(arr, R)); 


// Array17. n ta elementdan tashkil topgan massiv berilgan. Massiv elementlari orasidan yig'indisi eng katta bo'ladigan 2 ta qo'shni elementni chiqaruvchi programma tuzilsin.

// function Fr(arr) {
//     let first = -Infinity; 
//     let second = -Infinity; 
//     for (let i = 0; i < arr.length; i++) {
//       let current = arr[i];
//       if (current > first) {
//         second = first;
//         first = current;
//       } 
//       else if (current > second && current != first) {
//         second = current;
//       }
//     }
    
//     return [first, second];
// }
// let arr = [1, 5, 2, 8, 3, 9, 6];
// let result = Fr(arr);
// console.log(result); 
  

// Array18. n ta elementdan tashkil topgan massiv berilgan. Massiv elementlari orasida aniq 2 ta bir xil qiymatli element bor. Shu bir xil qiymatli elementlar indeksini chiqaruvchi programma tuzilsin.

// function DupArr(arr) {
//     let result = [];
//     arr.forEach((item, index) => {
//       if (arr.indexOf(item, index + 1) !== -1) {
//         if (result.indexOf(item) === -1) {
//           result.push(item);
//         }
//       }
//     });
//     return result.map((item) => arr.indexOf(item));
// }
// const arr = [1, 2, 3, 4, 5, 2, 6, 7, 8, 8, 9, 10];
// const indices = DupArr(arr);
// console.log(indices); 

//Array19. n ta elementdan tashkil topgan massiv berilgan. Massivda eng ko'p qatnashgan bir xil qiymatli elementni va uning sonini chiqaruvchi programma tuzilsin.

// function Maxg(arr) {
//     let count = 0;
//     let maxCount = 0;
//     let max = arr[0];
  
//     for (let i = 0; i < arr.length; i++) {
//       count = 1;
//       for (let j = i + 1; j < arr.length; j++) {
//         if (arr[i] === arr[j]) {
//           count++;
//         }
//       }
//       if (count > maxCount) {
//         maxCount = count;
//         max = arr[i];
//       }
//     }
    
//     console.log(`Eng ko'p qatnashgan element: ${max}`);
//     console.log(`Element soni: ${maxCount}`);
// }
// const arr = [1, 3, 5, 1, 2, 5, 3, 5, 2, 5, 5];
// Maxg(arr); 

// Array20. n ta elementdan iborat butun sonlardan tashkil topgan a massiv berilgan. a massivning juft elementlaridan tashkil topgan b massivini hosil qiling. b massiv elementlari soni va elementlari chiqarilsin.

// function getEvenElements(arr) {
//     let b = [];
//     for (let i = 0; i < arr.length; i++) {
//       if (arr[i] % 2 === 0) {
//         b.push(arr[i]);
//       }
//     }
//     console.log(`b massivining elementlari: ${b}`);
//     console.log(`b massivining uzunligi: ${b.length}`);
// }
// let a = [1, 2, 3, 4, 5, 6, 7, 8];
// getEvenElements(a);

// Array21. n ta butun sonlardan iborat a massiv va k butun soni berilgan. Massivning har bir elementini k ga orttiruvchi programma tuzilsin.

// function increa(arr, k) {
//     for (let i = 0; i < arr.length; i++) {
//       arr[i] += k;
//     }
//     return arr;
// }
// let a = [1, 2, 3, 4, 5];
// let k = 3;  
// console.log(increa(a, k));

// Array22. n ta elementdan iborat massiv berilgan (n juft son). Massivning birinchi yarmi va ikkinchi yarmi qiymatlari almashtirilsin.

// function AlishArray(arr) {
//     const half = arr.length / 2;
//     for (let i = 0; i < half; i++) {
//       const temp = arr[i];
//       arr[i] = arr[half + i];
//       arr[half + i] = temp;
//     }
//     return arr;
// }
// const arr = [1, 2, 3, 4, 5, 6];
// console.log(AlishArray(arr))

// Array23. n ta elementdan iborat massiv berilgan. Massivning elementlari teskari tartibda joylashtirilsin. (DIQQAT: Sizdan teskari tartibda chiqarish talab qilinayotgani yo'q. a[0] element a[n-1] bilan almashsin, va hakazo, teskari tartibda joylashtirish talab qilinyapti)

// function reversArr(arr) {
//     let reversedArr = [];
//     for (let i = arr.length - 1; i >= 0; i--) {
//       reversedArr.push(arr[i]);
//     }
//     return reversedArr;
// }
// let arr = [1, 2, 3, 4, 5];
// let reversedArr = reversArr(arr);
// console.log(reversedArr);

// Array24. n ta butun sonlardan iborat a massiv berilgan. Massivning eng kichik va eng katta elementlari orasidagilarini nolga almashtiruvchi programma tuzilsin. Eng kichik va eng katta elementlari o'zgarishsiz qoldirilsin.

// function ZeroArra(arr) {
//     var max = arr[0];
//     var min = arr[0];
//     for (var i = 1; i < arr.length; i++) {
//       if (arr[i] > max) {
//         max = arr[i];
//       }
//       if (arr[i] < min) {
//         min = arr[i];
//       }
//     }
//     for (var i = 0; i < arr.length; i++) {
//       if (arr[i] !== max && arr[i] !== min) {
//         arr[i] = 0;
//       }
//     }
//     return arr;
// }
// var arr = [1, 5, 3, 2, 7, 4];
// console.log(ZeroArra(arr)); 
  
// Array25. n ta elementdan tashkil topgan massiv berilgan. Massiv elementlarini bir qadam chapga siklik siljituvchi programma tuzilsin. a[n-1] element qiymati a[n-2] ga o'tadi, a[n-2] esa a[n-3] ga, ... a[0] esa a [n-1] ga o'tadi.

// function Yon(arr) {
//     var last = arr[arr.length - 1];
//     for (var i = arr.length - 1; i > 0; i--) {
//       arr[i] = arr[i - 1];
//     }
//     arr[0] = last;
//     return arr;
// }
// var arr = [1, 2, 3, 4, 5];
// Yon(arr);
// console.log(arr); 
  
// Array26. n ta elementdan tashkil topgan massiv va k butun soni berilgan (0<= k < n). Indeksi k ga teng bo'lgan elementni o'chiruvchi va yangi massiv qaytaruvchi deleteElementWithIndex(arr, k) nomli funksiya tuzilsin.

// function elementD(arr, k) {
//     arr.splice(k, 1);
//     return arr;
// }
// var arr = [1, 2, 3, 4, 5];
// elementD(arr, 2);
// console.log(arr);

// Array27. n ta elementdan tashkil topgan massiv va k, m butun sonlari berilgan (0< k < m < n). Indeksi k dan m gacha bo'lgan elementlarni o'chiruvchi programma tuzilsin. Hosil bo'lgan massiv elementlar soni va elementlari chiqarilsin.

// function elemntD(arr, k, m) {
//     arr.splice(k, m - k + 1);
//     console.log("Yangi massiv elementlari:", arr);
//     console.log("Yangi massiv uzunligi:", arr.length);
// }
// var arr = [1, 2, 3, 4, 5];
// elemntd(arr, 1, 3);

// Array28. n ta elementdan tashkil topgan massiv berilgan. Massiv elementlari orasidan bir xil qo'shnilarning birini o'chiruvchi programma tuzilsin.

// function Rem(arr) {
//     var uniqueArr = [];
//     for (var i = 0; i < arr.length; i++) {
//       if (uniqueArr.indexOf(arr[i]) === -1) {
//         uniqueArr.push(arr[i]);
//       }
//     }
//     console.log("Yangi massiv elementlari:", uniqueArr);
//     console.log("Yangi massiv uzunligi:", uniqueArr.length);
// }
// var arr = [1, 2, 3, 2, 4, 3, 5];
// Rem(arr);
  
// Array29. n ta elementdan tashkil topgan massiv berilgan. Massiv elementlari orasidan bir xil qiymatga ega bo'lganlarini o'chiruvchi programma tuzilsin.

// function removeDuplicates(arr) {
//     var uniqueArr = [];
//     for (var i = 0; i < arr.length; i++) {
//       if (arr.lastIndexOf(arr[i]) === arr.indexOf(arr[i])) {
//         uniqueArr.push(arr[i]);
//       }
//     }
//     console.log("Yangi massiv elementlari:", uniqueArr);
// }
// var arr = [1, 5, 6, 1, 5, 7, 2];
// removeDuplicates(arr);
  
// Array30. arr nomli massivda k qiymatli bir nech element mavjude. Shu elementlar indekslari massividan iborat qiymat qaytaruvchid searchAllElements(arr) nomli funksiya hosil qiling !

// function Selemnt(arr, k) {
//     var index = [];
//     for (var i = 0; i < arr.length; i++) {
//       if (arr[i] === k) {
//         index.push(i);
//       }
//     }
//     console.log("Qiymatga teng elementlar indekslari:", index);
//     return index;
//   }
// var arr = [1, 5, 6, 1, 5, 7, 2];
// var k = 5;
// Selemnt(arr, k);